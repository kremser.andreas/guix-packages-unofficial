(use-modules  (guix packages)
              (guix download)
              (guix build-system gnu)
              (guix licenses)
              )
(package
 (name "unifdef")
 (version "2.12")
 (source (origin
          (method url-fetch)
          (uri (string-append "https://dotat.at/prog/unifdef/unifdef-" version ".tar.gz"))
          (sha256
           (base32 "02yig3ibckh2kvhi92wybqp90yv2f34klw99jfz7xfdp9ni699gv" ))))
 (build-system gnu-build-system)
 (arguments '(#:tests? #f
              #:make-flags (list (string-append "DESTDIR=" (assoc-ref %outputs "out"))
                                 "prefix=/")
              #:phases (modify-phases %standard-phases
                                      (replace 'configure (lambda _ (setenv "CC" (which "gcc")))
                                               ))))
 (synopsis "unifdef")
 (description "unifdef")
 (home-page "https://dotat.at/prog/unifdef/unifdef-")
 (license #f)
 )


