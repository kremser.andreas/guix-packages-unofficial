(use-modules  (guix packages)
              (guix download)
              (guix build-system gnu)
              (guix licenses)
              (gnu packages attr)
              (gnu packages acl)
              (gnu packages linux)
              )
(package
 (name "bfs")
 (version "1.6")
 (source (origin
          (method url-fetch)
          (uri (string-append "https://github.com/tavianator/bfs/archive/" version ".tar.gz"))
          (sha256
           (base32 "1yr2jzarx3zr368c7jc6l850g0kdzh5smqbl37c90gqcc86p4wc3" ))))
 (build-system gnu-build-system)
 (inputs `(("attr", attr)
           ("acl", acl)
           ("libcap", libcap)
           ))
 (arguments '(#:tests? #f
              #:make-flags (list (string-append "DESTDIR=" (assoc-ref %outputs "out"))
                                 "PREFIX=/")
              #:phases (modify-phases %standard-phases
                                      (replace 'configure (lambda _ (setenv "CC" (which "gcc")))
                                               ))))
 (synopsis "bfs, breadth first search alternative to find")
 (description "bfs")
 (home-page "https://github.com/tavianator/bfs/archive/")
 (license #f)
 )


;;guix package --install-from-file=bfs.scm
